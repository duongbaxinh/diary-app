import 'package:diary_app/components/svg_icon.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/pages/gallery/index.dart';
import 'package:diary_app/pages/home_page/index.dart';
import 'package:diary_app/pages/mine_page/index.dart';
import 'package:diary_app/pages/my_diary/index.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    RxInt selected = 1.obs;
    void onchangePage(int index) => {selected.value = index};
    List<Map<String, dynamic>> icons = [
      {"id": 0, "icon": AppImage.diary},
      {"id": 1, "icon": AppImage.calendar},
      {"id": 2, "icon": AppImage.add},
      {"id": 3, "icon": AppImage.photo},
      {"id": 4, "icon": AppImage.account},
    ];
    List<Widget> pages = [
      const HomePage(),
      const MyDiary(),
      const HomePage(),
      const Gallery(),
      const MinePage(),
    ];
    final HomeController controller = Get.put(HomeController());
    return Obx(() => Scaffold(
          body: Stack(
            children: [
              Image(
                image: AssetImage(controller.theme['background']),
                width: AppSize.maxW(context),
                height: AppSize.maxH(context),
                fit: BoxFit.fill,
              ),
              pages[selected.value],
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: AppSize.maxW(context),
                  height: 80,
                  decoration: BoxDecoration(
                    color: AppColor.bgColor,
                    borderRadius: AppBorder.borderTLTR,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (final iconData in icons)
                          SvgIcon(
                              color: selected == iconData['id']
                                  ? AppColor.primaryColor
                                  : AppColor.secondColor,
                              special: iconData['id'] == 2,
                              icon: iconData['icon'],
                              onHandle: () => onchangePage(iconData['id'])),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
