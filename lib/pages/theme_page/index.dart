import 'package:carousel_slider/carousel_slider.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/pages/home_page/index.dart';
import 'package:diary_app/pages/main_page/index.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ThemePage extends StatelessWidget {
  final int initTheme;
  const ThemePage({super.key, this.initTheme = 0});

  @override
  Widget build(BuildContext context) {
    final HomeController ctrl = Get.find();
    RxList<RxString> backgroundList = [
      AppImage.background_01,
      AppImage.background_02,
      AppImage.background_03,
      AppImage.background_04,
      AppImage.background_05,
    ].obs;
    RxString currentBackground = backgroundList.value[initTheme];
    RxInt pageSelected = 0.obs;

    return Obx(() => Stack(
          children: [
            Image.asset(
              currentBackground.value,
              width: AppSize.maxW(context),
              height: AppSize.maxH(context),
              fit: BoxFit.fill,
            ),
            Scaffold(
                appBar: AppBar(
                  iconTheme: const IconThemeData(color: Colors.white),
                  title: Text(
                    'App theme',
                    style: TextStyle(
                        fontSize: AppText.heading1,
                        fontWeight: AppText.w1,
                        color: Colors.white),
                  ),
                  backgroundColor: AppColor.transparent,
                  centerTitle: true,
                ),
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 500,
                        child: CarouselSlider.builder(
                            itemCount: backgroundList.value.length,
                            itemBuilder: (context, index, realIndex) =>
                                Container(
                                  width: 260,
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20)),
                                      border: Border.all(
                                          color: AppColor.primaryColor,
                                          width: 3)),
                                  child: ClipRRect(
                                    borderRadius: AppBorder.radius1,
                                    child: Image.asset(
                                      backgroundList.value[index].value,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                            options: CarouselOptions(
                              initialPage: initTheme,
                              height: 450,
                              enableInfiniteScroll: false,
                              onPageChanged: (index, reason) {
                                pageSelected.value = index;
                                currentBackground.value =
                                    backgroundList[index].value;
                              },
                            )),
                      ),
                      Container(
                          width: 250,
                          decoration: const BoxDecoration(
                              color: AppColor.primaryColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      AppColor.transparent),
                                  elevation: MaterialStateProperty.all(0)),
                              onPressed: () {
                                ctrl.changeBackground(pageSelected.value);
                                Get.offAll(const MainPage());
                              },
                              child: Text(
                                'Applize',
                                style: TextStyle(
                                    fontWeight: AppText.w1,
                                    fontSize: AppText.heading3,
                                    color: Colors.white),
                              )))
                    ],
                  ),
                )),
          ],
        ));
  }
}
