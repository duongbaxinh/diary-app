import 'package:diary_app/components/day_item.dart';
import 'package:diary_app/components/diary_item.dart';
import 'package:diary_app/components/wheel_scrollview.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:diary_app/pages/detail_page/index.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:diary_app/utils/filter_day.dart';
import 'package:diary_app/utils/format_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../styles/app_color.dart';

class MyDiary extends StatelessWidget {
  const MyDiary({super.key});
  @override
  Widget build(BuildContext context) {
    final HomeController homeController = Get.find();
    final List<Diary> diaries = homeController.listDiary;
    Rx<DateTime> focusedDay = DateTime.now().obs;
    Rx<DateTime> today = DateTime.now().obs;
    void changeDate(type, dynamic value) {
      if (type == 'changeMonth') {
        focusedDay.value =
            DateTime(focusedDay.value.year, value as int, focusedDay.value.day);
      } else {
        focusedDay.value = DateTime(value.year, value.month, value.day);
      }
    }

    return Obx(() => Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            width: AppSize.maxW(context),
            height: AppSize.maxH(context),
            padding:
                const EdgeInsets.only(left: 15, top: 25, right: 15, bottom: 80),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: AppSize.maxW(context),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Get.bottomSheet(Container(
                              padding: const EdgeInsets.all(30),
                              width: AppSize.maxW(context),
                              height: AppSize.maxH(context) / 2,
                              color: AppColor.bgColor,
                              child: Column(
                                children: [
                                  Text(
                                    'Select date',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: AppText.heading1,
                                        fontWeight: AppText.w1),
                                  ),
                                  WheelScrollView(onChangeDate: changeDate),
                                ],
                              ),
                            ));
                          },
                          child: Text(
                            FormatDate.monthName(focusedDay.value),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: AppColor.bgColor,
                                fontFamily: AppText.mainFontFamily,
                                fontSize: AppText.heading1,
                                fontWeight: AppText.w1),
                          )),
                      GestureDetector(
                          onTap: () => {changeDate('', today.value)},
                          child: Icon(Icons.calendar_today))
                    ],
                  ),
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(0),
                    children: [
                      SizedBox(
                        width: AppSize.maxW(context),
                        height: 350,
                        child: TableCalendarBase(
                          firstDay: DateTime.utc(2020, 01, 01),
                          lastDay: DateTime.utc(2100, 01, 01),
                          focusedDay: focusedDay.value,
                          dayBuilder: (context, day, focusedDay) {
                            print('checkdate ${day.month == focusedDay.month}');
                            bool daySelected = day.day == focusedDay.day;
                            return day.month == focusedDay.month
                                ? GestureDetector(
                                    onTap: () {
                                      changeDate('day', day);
                                    },
                                    child: DayItem(
                                      daySelected: daySelected,
                                      day: day,
                                      diaries: diaries,
                                    ),
                                  )
                                : Container(
                                    child: Text(''),
                                  );
                          },
                          rowHeight: 10,
                          dowVisible: true,
                          dowHeight: 50,
                          dowBuilder: (context, day) {
                            return Center(
                              child: Text(
                                FormatDate.dayName(day).substring(0, 1),
                                style: const TextStyle(
                                  color: AppColor.bgColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            );
                          },
                          onPageChanged: (date) => changeDate('', date),
                        ),
                      ),
                      if (diaries.isNotEmpty)
                        Column(
                          children: [
                            for (final das in ProcessingDate.dias(
                                focusedDay.value, diaries))
                              GestureDetector(
                                onTap: () {
                                  Get.to(DetailDiary(
                                    diary: das,
                                  ));
                                },
                                child: DiaryItem(diary: das),
                              )
                          ],
                        )
                      else
                        const Text('not thing ')
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
