import 'dart:async';

import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/pages/validate_page/index.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../main_page/index.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  var isLoading = true;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            backgroundColor: AppColor.primaryColor,
            body: isLoading
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 250,
                            height: 250,
                            child: Image.asset(
                              AppImage.icon_app,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : PinPage()));
  }
}
