import 'package:diary_app/components/diary_item.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/pages/detail_page/index.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

import '../../model/Diary.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final HomeController controller = Get.find();
    RxList<Diary> diaries = controller.listDiary;
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text(
          'My diary',
          style: TextStyle(
              color: AppColor.whiteColor,
              fontFamily: AppText.mainFontFamily,
              fontSize: AppText.heading1,
              fontWeight: AppText.w1),
        ),
        backgroundColor: AppColor.transparent,
      ),
      body: Padding(
          padding: AppBorder.padding_02,
          child: Obx(
            () {
              return ListView.builder(
                itemBuilder: (context, index) {
                  if (diaries.value.isNotEmpty) {
                    return Slidable(
                      key: const ValueKey(1),
                      endActionPane: ActionPane(
                        motion: const ScrollMotion(),
                        children: [
                          SlidableAction(
                            onPressed: (z) {
                              controller.deleteDiary(diaries[index].id);
                              // Get.reload();
                            },
                            padding: AppBorder.padding_00,
                            backgroundColor: const Color(0xFFFE4A49),
                            borderRadius: const BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20)),
                            foregroundColor: Colors.white,
                            icon: Icons.delete,
                            label: 'Delete',
                          ),
                        ],
                      ),
                      child: GestureDetector(
                        onTap: () =>
                            Get.to(DetailDiary(diary: diaries.value[index])),
                        child: DiaryItem(diary: diaries.value[index]),
                      ),
                    );
                  } else {
                    return Center(
                      child: Text(
                        "You haven't diary",
                        style: TextStyle(
                            fontWeight: AppText.w3, color: AppColor.whiteColor),
                      ),
                    );
                  }
                },
                itemCount: diaries.value.length,
              );
            },
          )),
    );
  }
}
