import 'package:flutter/material.dart';

class LanguageSelectionPage extends StatefulWidget {
  @override
  _LanguageSelectionPageState createState() => _LanguageSelectionPageState();
}

class _LanguageSelectionPageState extends State<LanguageSelectionPage> {
  Set<String> selectedLanguages = {};

  List<String> languages = [
    'Vietnamese',
    'English',
    'Spanish',
    'French',
    'German',
    'Chinese',
    'Japanese',
    'Italian',
    'Russian',
    'Arabic',
    'Portuguese',
    'Dutch',
    'Swedish',
    'Korean',
    'Turkish',
    'Polish',
    'Indonesian',
    'Greek',
    'Thai',
    'Czech',
    'Danish',
    'Finnish',
    'Norwegian',
    'Hungarian',
    'Romanian',
    'Hebrew',
    'Hindi',
    'Bengali',
    'Punjabi',
    'Telugu',
    'Tamil',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink[100],
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 80.0),
            child: Card(
              margin: EdgeInsets.all(10.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: ListView.builder(
                itemCount: languages.length,
                itemBuilder: (BuildContext context, int index) {
                  final language = languages[index];
                  final isSelected = selectedLanguages.contains(language);
                  return ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: RoundCheckbox(
                      value: isSelected,
                      onTap: () {
                        setState(() {
                          if (isSelected) {
                            selectedLanguages.remove(language);
                          } else {
                            selectedLanguages = {language};
                          }
                        });
                      },
                    ),
                    title: Text(
                      language,
                      style: TextStyle(color: Colors.black87), // Màu xám
                    ),
                  );
                },
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).padding.top,
            left: 0,
            right: 0,
            child: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                  // Xử lý khi bấm nút back
                },
              ),
              title: Text(
                'Language',
                textAlign: TextAlign.center,

                style: TextStyle(fontWeight: FontWeight.bold ,color: Colors.black), // Chữ đậm
              ),
              centerTitle: true,
            ),
          ),
        ],
      ),
    );
  }
}

class RoundCheckbox extends StatelessWidget {
  final bool value;
  final VoidCallback onTap;

  RoundCheckbox({required this.value, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 22.0,
        height: 22.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            width: 2.0,
            color: Colors.black12,
          ),
          color: value ? Colors.redAccent : Colors.transparent,
        ),
        child: value
            ? Icon(
          Icons.radio_button_unchecked_outlined,
          size: 18.0,
          color: Colors.white,
        )
            : null,
      ),
    );
  }
}
