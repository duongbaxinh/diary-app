import 'package:diary_app/components/title_header.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/pages/language_page/index.dart';
import 'package:diary_app/pages/pass_code/index.dart';
import 'package:diary_app/pages/theme_page/index.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MinePage extends StatelessWidget {
  const MinePage({super.key});

  @override
  Widget build(BuildContext context) {
    List<RxString> themes = [
      AppImage.background_01,
      AppImage.background_02,
      AppImage.background_03,
      AppImage.background_04,
      AppImage.background_05,
    ];
    List<Map<String, dynamic>> listOptions = [
      {
        "id": 1,
        "icon": AppImage.password,
        "title": "Passworde",
        "toPage": Passcode()
      },
      {"id": 2, "icon": AppImage.mood, "title": "Mood gallery", "toPage": ''},
      {
        "id": 3,
        "icon": AppImage.mood,
        "title": "Skip mood selection",
        "toPage": ''
      },
      {
        "id": 4,
        "icon": AppImage.language,
        "title": "Language",
        "toPage": LanguageSelectionPage()
      },
      {"id": 5, "icon": AppImage.mood, "title": "Date & time", "toPage": ''},
      {
        "id": 6,
        "icon": AppImage.password,
        "title": "Change app icon",
        "toPage": ''
      },
    ];
    List<Map<String, dynamic>> listOptions2 = [
      {"id": 1, "icon": AppImage.faq, "title": "FAQ", "toPage": ''},
      {
        "id": 2,
        "icon": AppImage.policy,
        "title": "Privacy policy",
        "toPage": ''
      },
      {"id": 3, "icon": AppImage.feedback, "title": "Feedback", "toPage": ''},
      {"id": 4, "icon": AppImage.rate, "title": "Rate us", "toPage": ''},
    ];
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text(
            'Mine',
            style: TextStyle(
                color: AppColor.whiteColor,
                fontFamily: AppText.mainFontFamily,
                fontSize: AppText.heading1,
                fontWeight: AppText.w1),
          ),
          backgroundColor: AppColor.transparent,
        ),
        body: Padding(
          padding: AppBorder.padding_02,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(bottom: 100),
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                width: AppSize.maxW(context),
                height: 180,
                decoration: BoxDecoration(
                  color: AppColor.bgColor,
                  borderRadius: AppBorder.radius1,
                ),
                child: Column(
                  children: [
                    const TitleHeader(
                      title: 'App theme',
                      icon: '',
                      isHeader: true,
                    ),
                    Expanded(
                      child: ListView.separated(
                          padding: const EdgeInsets.all(0),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                Get.to(ThemePage(initTheme: index));
                              },
                              child: SizedBox(
                                width: 80,
                                height: 100,
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  child: Image.asset(
                                    themes[index].value,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (context, index) => const SizedBox(
                                width: 10,
                              ),
                          itemCount: themes.length),
                    ),
                  ],
                ),
              ),
              AppSize.sby(20),
              Container(
                padding: const EdgeInsets.all(10),
                width: AppSize.maxW(context),
                decoration: BoxDecoration(
                  color: AppColor.bgColor,
                  borderRadius: AppBorder.radius1,
                ),
                child: Column(
                  children: [
                    const TitleHeader(
                        title: 'Notification', isHeader: true, icon: ''),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '20:00',
                          style: TextStyle(
                              fontSize: AppText.heading1,
                              fontWeight: AppText.w1),
                        ),
                        Switch(value: true, onChanged: (value) => print(value))
                      ],
                    )
                  ],
                ),
              ),
              AppSize.sby(20),
              Container(
                padding: AppBorder.padding_03,
                width: AppSize.maxW(context),
                decoration: BoxDecoration(
                  color: AppColor.bgColor,
                  borderRadius: AppBorder.radius1,
                ),
                child: Column(
                  children: [
                    for (final option in listOptions)
                      GestureDetector(
                        onTap: () {
                          Get.to(option['toPage']);
                        },
                        child: TitleHeader(
                          leading: option['icon'],
                          title: option['title'],
                          icon: '',
                        ),
                      )
                  ],
                ),
              ),
              AppSize.sby(20),
              Container(
                padding: AppBorder.padding_03,
                width: AppSize.maxW(context),
                decoration: BoxDecoration(
                  color: AppColor.bgColor,
                  borderRadius: AppBorder.radius1,
                ),
                child: Column(
                  children: [
                    for (final option in listOptions2)
                      TitleHeader(
                        leading: option['icon'],
                        title: option['title'],
                        icon: '',
                      )
                  ],
                ),
              ),
              AppSize.sby(20),
              Container(
                padding: AppBorder.padding_01_03,
                width: AppSize.maxW(context),
                decoration: BoxDecoration(
                  color: AppColor.bgColor,
                  borderRadius: AppBorder.radius1,
                ),
                child: TitleHeader(
                  leading: AppImage.trash,
                  title: "Delete all data",
                  icon: '',
                ),
              ),
            ],
          ),
        ));
  }
}
