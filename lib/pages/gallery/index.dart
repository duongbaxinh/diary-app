import 'dart:io';

import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Gallery extends StatelessWidget {
  const Gallery({super.key});

  @override
  Widget build(BuildContext context) {
    final HomeController homeController = Get.find();
    List imgs =
        homeController.listDiary.expand((element) => element.images).toList();
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text(
          'Galery',
          style: TextStyle(
              color: AppColor.whiteColor,
              fontFamily: AppText.mainFontFamily,
              fontSize: AppText.heading1,
              fontWeight: AppText.w1),
        ),
        backgroundColor: AppColor.transparent,
      ),
      body: SingleChildScrollView(
        padding: AppBorder.padding_02,
        child: Center(
          child: Wrap(
            spacing: 10,
            runSpacing: 10,
            children: [
              for (var img in imgs)
                SizedBox(
                  width: 100,
                  child: Image.file(File(img)),
                )
            ],
          ),
        ),
      ),
    );
  }
}
