import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
class Passcode extends StatefulWidget {
  const Passcode({super.key});

  @override
  State<Passcode> createState() => _PasscodeState();
}

class _PasscodeState extends State<Passcode> {
  bool _isSwitched = false;
  bool _isSwitched2 = false;
  bool _isCheck = true;
  bool _isCheck2 = true;
  void _toggleIcon() {
    setState(() {
      _isCheck = !_isCheck;
    });
  }
  void _toggleIcon2() {
    setState(() {
      _isCheck2 = !_isCheck2;
    });
  }


  @override
  Widget build(BuildContext context) {
    Size mq = MediaQuery.sizeOf(context);
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,     // Bắt đầu từ góc trên bên trái
              end: Alignment.bottomLeft,   // Kết thúc tại góc dưới bên phải
              colors: [
                Colors.blue.withOpacity(1),
                Colors.pinkAccent.withOpacity(1),
                Colors.white.withOpacity(0.7),
              ],
              stops: [0.0, 0.5, 1.0],
            )
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.arrow_back_ios_new,
                          color: Colors.black,),
                        onPressed: () {
                          Navigator.pop(context);
                          print('Button pressed!');
                        },

                      ),
                      Expanded(
                        child: Text(
                          'Passcode',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,

                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                    width: mq.width,
                    height: 280,
                    margin: EdgeInsets.only(top: mq.height * 0.05),
                    padding: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20), // Red border around the circle
                    ),
                    child: Column(
                      children: [

                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Passcode protect',
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,

                                ),),
                              Center(
                                child: Transform.scale(
                                  scale: 1, // Adjust the scale factor here to increase or decrease the size
                                  child: Switch(
                                    value: _isSwitched,
                                    onChanged: (value) {
                                      setState(() {
                                        _isSwitched = value;
                                      });
                                    },
                                    activeTrackColor: Colors.pinkAccent,
                                    activeColor: Colors.pink,
                                    inactiveThumbColor: Colors.grey,
                                    inactiveTrackColor: Colors.grey[100],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            width: 800,
                            height: 120,
                            decoration: BoxDecoration(
                              color: Colors.blue.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(15), // Red border around the circle
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(13.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('PIN',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,

                                        ),),
                                      InkWell(
                                        onTap: _toggleIcon,
                                        child: Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child:Icon(
                                            _isCheck ? Icons.radio_button_unchecked : Icons.radio_button_checked_sharp,  // Chọn icon dựa trên trạng thái
                                            size: 25.0,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),


                                    ],
                                  ),

                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Pattern',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,

                                        ),),
                                      InkWell(
                                        onTap: _toggleIcon2,
                                        child: Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child:Icon(
                                            _isCheck2 ? Icons.radio_button_unchecked : Icons.radio_button_checked_sharp,  // Chọn icon dựa trên trạng thái
                                            size: 25.0,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),
                                      // ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),

                        ),
                        SizedBox(height: 10,),
                        Text('Change passcode',style: TextStyle(
                          color: Colors.pink,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,

                        ),)
                      ],
                    ),
                  ),
                  SizedBox(height: 30,),
                  Row(
                    children: [
                      SizedBox(width: 30,),
                      Align(
                        alignment: Alignment.centerLeft,  // Căn chỉnh text về bên trái
                        child: Text('Security',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.blueGrey,
                          ),),
                      ),
                    ],
                  ),
                  Container(
                    width: mq.width,
                    height: 120,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20), // Red border around the circle
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,  // Căn chỉnh text về bên trái
                            child: Text('Set security question',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),),
                          ),
                          SizedBox(height: 10,),

                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 1,
                            color: Colors.grey.withOpacity(0.7),
                          ),
                          SizedBox(height: 10,),
                          Align(
                            alignment: Alignment.centerLeft,  // Căn chỉnh text về bên trái
                            child: Text('Set verification email',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,

                              ),),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 25,),
                  Container(
                    width: mq.width,
                    height: 60,
                    padding: EdgeInsets.all(20.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20), // Red border around the circle
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Enable fingerprint',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,

                          ),),
                        Center(
                          child: Transform.scale(
                            scale: 1,
                            child: Switch(
                              value: _isSwitched2,
                              onChanged: (value) {
                                setState(() {
                                  _isSwitched2 = value;
                                });
                              },
                              activeTrackColor: Colors.pinkAccent,
                              activeColor: Colors.pink,
                              inactiveThumbColor: Colors.grey,
                              inactiveTrackColor: Colors.grey[100],
                            ),
                          ),
                        ),
                      ],
                    ),


                  ),
                  SizedBox(height: 300,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
