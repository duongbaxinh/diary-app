import 'dart:io';

import 'package:diary_app/components/svg_icon.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:diary_app/pages/edit_diary/index%20.dart';
import 'package:diary_app/pages/theme_page/index.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/utils/format_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../components/title_date.dart';
import '../../styles/app_size.dart';
import '../../styles/app_text.dart';

class DetailDiary extends StatelessWidget {
  final Diary diary;
  const DetailDiary({super.key, required this.diary});

  @override
  Widget build(BuildContext context) {
    final HomeController controller = Get.find();
    return Stack(
      children: [
        Image(
          image: AssetImage(controller.theme['background']),
          width: AppSize.maxW(context),
          height: AppSize.maxH(context),
          fit: BoxFit.fill,
        ),
        Scaffold(
          appBar: AppBar(
            title: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Detail',
                  style: TextStyle(
                      color: AppColor.whiteColor,
                      fontFamily: AppText.mainFontFamily,
                      fontSize: AppText.heading1,
                      fontWeight: AppText.w1),
                ),
                SvgIcon(
                    color: AppColor.whiteColor,
                    icon: AppImage.edit,
                    onHandle: () {
                      Get.to(EditDiary(diaryData: diary));
                    })
              ],
            ),
            actions: [
              GestureDetector(
                onTap: () {},
                child: Icon(Icons.edit_square),
              )
            ],
            backgroundColor: AppColor.transparent,
          ),
          body: Padding(
            padding: AppBorder.padding_02,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    contentPadding: AppBorder.padding_00,
                    leading: Text(
                      diary.date.day.toString(),
                      style: TextStyle(
                          color: AppColor.whiteColor,
                          fontFamily: AppText.mainFontFamily,
                          fontWeight: AppText.w1,
                          fontSize: AppText.heading1),
                    ),
                    title: Text(
                      FormatDate.monthName(diary.date) +
                          ' ' +
                          diary.date.year.toString(),
                      style: TextStyle(
                          color: AppColor.whiteColor,
                          fontFamily: AppText.mainFontFamily,
                          fontWeight: AppText.w1,
                          fontSize: AppText.heading1),
                    ),
                    subtitle: Text(
                      FormatDate.dayName(diary.date),
                      style: TextStyle(
                          color: AppColor.whiteColor,
                          fontFamily: AppText.mainFontFamily,
                          fontWeight: AppText.w1,
                          fontSize: AppText.heading1),
                    ),
                  ),
                  Text(
                    diary.title,
                    style: TextStyle(
                        color: AppColor.whiteColor,
                        fontFamily: AppText.mainFontFamily,
                        fontWeight: AppText.w1,
                        fontSize: AppText.heading1),
                  ),
                  Text(
                    diary.content,
                    style: TextStyle(
                        color: AppColor.whiteColor,
                        fontFamily: AppText.mainFontFamily,
                        fontWeight: AppText.w1,
                        fontSize: AppText.heading1),
                  ),
                  diary.images.isNotEmpty
                      ? Wrap(
                          spacing: 10,
                          children: [
                            for (var image in diary.images)
                              SizedBox(
                                  width: 150,
                                  child: Image.file(
                                    File(image),
                                    fit: BoxFit.fill,
                                  ))
                          ],
                        )
                      : Text('')
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
