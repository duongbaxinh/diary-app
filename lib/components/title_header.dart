import 'package:diary_app/components/svg_icon.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';

class TitleHeader extends StatelessWidget {
  final String title;
  final bool isHeader;
  final String leading;
  final String icon;
  const TitleHeader(
      {super.key,
      required this.title,
      required this.icon,
      this.leading = '',
      this.isHeader = false});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.all(0),
      dense: true,
      leading: leading.isNotEmpty
          ? Container(
              width: 30,
              height: 30,
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  border: Border.all(color: AppColor.darkGreyColor, width: 0.5),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: SvgIcon(
                  icon: leading, color: AppColor.orangeColor, onHandle: () {}))
          : null,
      title: Text(
        title,
        style: TextStyle(
            fontSize: isHeader ? AppText.heading2 : AppText.heading4,
            fontWeight: AppText.w1),
      ),
      trailing: const Icon(Icons.arrow_forward_ios),
    );
  }
}
