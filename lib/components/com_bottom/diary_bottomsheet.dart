import 'dart:io';

import 'package:diary_app/components/com_bottom/bg_bottomsheet.dart';
import 'package:diary_app/components/com_bottom/doc_bottomsheet.dart';
import 'package:diary_app/components/com_bottom/icon_bottomsheet.dart';
import 'package:diary_app/components/com_bottom/text_bottomsheet.dart';
import 'package:diary_app/components/mini/TextInput.dart';
import 'package:diary_app/components/svg_icon.dart';
import 'package:diary_app/components/title_date.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/controller/quill_controller.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:diary_app/pages/main_page/index.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class DiaryBottomSheet extends StatelessWidget {
  const DiaryBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    //

    // ----------------------- controller ----------------
    HomeController controller = Get.find();
    QuillController quillController = Get.put(QuillController());

    // ---------------------
    Future<String> pickImage() async {
      final pickedImage =
          await ImagePicker().pickImage(source: ImageSource.gallery);
      if (pickedImage == null) return '';
      File image = File(pickedImage.path);
      return image.path;
    }

    // --------------------------------Rx value -----------------------------------------
    RxString imageStr = ''.obs;
    RxString emotion = ''.obs;
    RxInt count = 0.obs;
    RxList<dynamic> listImage = [].obs;
    RxMap<String, dynamic> bgDiary =
        {"bg": AppImage.background_01, "color": Colors.white}.obs;

    // --------------------------------Rx value -----------------------------------------
    final dynamic diary = Diary(date: DateTime.now());

    // --------------------------------- Data icon emotion --------------------------------
    final RxList<Map<String, dynamic>> listEmotion = [
      {"id": 1, "url": AppImage.faceIcon_01},
      {"id": 2, "url": AppImage.faceIcon_02},
      {"id": 3, "url": AppImage.faceIcon_03},
      {"id": 4, "url": AppImage.faceIcon_04},
      {"id": 5, "url": AppImage.faceIcon_05},
    ].obs;

    // ---------------------------------- Icon Sheeed Button -------------------------------
    final List<Map<String, dynamic>> listIconSheet = [
      {
        "icon": AppImage.background,
        "component": Container(
          height: 350,
          width: AppSize.maxW(context),
          padding: AppBorder.padding_01,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: AppBorder.borderTLTR),
          child: BgBottomSheet(
            title: "Background",
            bgDiary: bgDiary,
          ),
        )
      },
      {"icon": AppImage.photo, "component": null},
      {
        "icon": AppImage.emotion,
        "component": const IconBottomSheet(
          title: 'Icon',
        )
      },
      {
        "icon": AppImage.textChange,
        "component": Container(
          height: 250,
          child: DocBottomSheet(
            title: 'Font',
          ),
        )
      },
      {
        "icon": AppImage.tag,
        "component": Container(
          child: const Text('Tag'),
        )
      },
    ];

    return Container(
      width: AppSize.maxW(context),
      height: AppSize.maxH(context),
      decoration: const BoxDecoration(color: Colors.white),
      child: Stack(
        children: [
          Obx(
            () => SizedBox(
              width: AppSize.maxW(context),
              height: AppSize.maxH(context),
              child: Image.asset(
                controller.diaryModel['bg'],
                fit: BoxFit.fill,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Obx(() => Text(listImage.value.length.toString())),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: const Icon(
                        Icons.arrow_circle_left_outlined,
                        color: AppColor.whiteColor,
                        size: 30,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        controller.addOneWorkPlan();
                        Get.to(MainPage());
                      },
                      child: Chip(
                        label: const Text('Save'),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                      ),
                    )
                  ],
                ),
                Obx(() => TitleDate(
                      isChangeEmotion: true,
                      emotion: emotion.value,
                      fn: () {
                        Get.bottomSheet(Container(
                          width: 500,
                          height: 300,
                          color: Colors.red,
                          child: Column(
                            children: [
                              Text(
                                'How was your day?',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: AppText.w1,
                                    fontSize: AppText.heading1),
                              ),
                              AppSize.sby(20),
                              Wrap(
                                spacing: 20,
                                children: [
                                  for (final icon in listEmotion.value)
                                    GestureDetector(
                                      onTap: () {
                                        emotion.value = icon['url'];
                                        controller.updateDiaryDiaryModel(
                                            icon['url'], 'icon');
                                        Get.back();
                                      },
                                      child: Image.asset(
                                        icon['url'],
                                        width: 50,
                                        height: 50,
                                      ),
                                    )
                                ],
                              )
                            ],
                          ),
                        ));
                      },
                      color: bgDiary['color'],
                      diary: diary,
                    )),
                TextInput(
                  min_line: 1,
                  max_line: 1,
                  onChangeVa: (value) =>
                      controller.updateDiaryDiaryModel(value, "title"),
                  fontSize: AppText.heading1,
                  wContent: AppText.w1,
                  wHintext: AppText.w1,
                  color: bgDiary['color'],
                ),
                TextInput(
                  min_line: 1,
                  max_line: 1,
                  onChangeVa: (value) =>
                      controller.updateDiaryDiaryModel(value, "content"),
                  fontSize: AppText.heading1,
                  wContent: AppText.w1,
                  wHintext: AppText.w1,
                  color: bgDiary['color'],
                ),
                Obx(() => Container(
                      child: listImage.value.isNotEmpty
                          ? GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 4,
                                      mainAxisSpacing: 10,
                                      crossAxisSpacing: 10,
                                      childAspectRatio: 2 / 3),
                              itemBuilder: (context, index) => ClipRRect(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                child: Image.file(
                                  File(listImage.value[index]),
                                  fit: BoxFit.fill,
                                ),
                              ),
                              itemCount: listImage.value.length,
                            )
                          : null,
                    )),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              padding: AppBorder.padding_02,
              color: AppColor.whiteColor,
              width: AppSize.maxW(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  for (var item in listIconSheet)
                    item['component'] != null
                        ? SvgIcon(
                            color: Colors.red,
                            icon: item['icon'],
                            onHandle: () {
                              Get.bottomSheet(item['component']);
                            })
                        : Obx(() => Row(
                              children: [
                                SvgIcon(
                                    icon: AppImage.photo,
                                    onHandle: () async {
                                      String pathImage = await pickImage();
                                      listImage.add(pathImage);
                                      imageStr.value = pathImage;
                                      controller.updateDiaryDiaryModel(
                                          listImage.value, "image");
                                    }),
                                Text(
                                  '${listImage.value.length.toString()}',
                                )
                              ],
                            ))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
