import 'dart:io';

import 'package:diary_app/controller/main_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ImageBottomSheet extends StatelessWidget {
  const ImageBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    var image = File('').obs;
    Future<void> pickImage() async {
      final pickedImage =
          await ImagePicker().pickImage(source: ImageSource.gallery);
      if (pickedImage == null) return;
      image.value = File(pickedImage.path);
      controller.diaryModel['image'].add(image.value);
    }

    return Obx(() => Column(
          children: [
            image.value.path != ''
                ? Image.file(
                    image.value!,
                    width: 200,
                    height: 200,
                  )
                : Text('unload'),
            Container(
              child: ElevatedButton(
                child: Text('Image'),
                onPressed: () {
                  pickImage();
                },
              ),
            ),
          ],
        ));
  }
}
