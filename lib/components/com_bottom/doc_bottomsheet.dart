import 'package:diary_app/components/svg_icon.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/controller/quill_controller.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class DocBottomSheet extends StatelessWidget {
  final String title;
  const DocBottomSheet({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    final QuillController quillController = Get.find();
    final List<Color> colors = [
      AppColor.primaryColor,
      AppColor.secondColor,
      AppColor.darkGreyColor,
      AppColor.orangeColor
    ];
    final List<String> fonts = ['font_01', 'font_02'];

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: const Icon(Icons.arrow_drop_down_outlined),
        title: Text(
          title,
          style: TextStyle(
            fontSize: AppText.heading3,
            fontWeight: AppText.w3,
          ),
        ),
        actions: const [
          Chip(
            label: Icon(Icons.save, color: AppColor.whiteColor),
            backgroundColor: AppColor.primaryColor,
          )
        ],
      ),
      body: Container(
        color: AppColor.whiteColor,
        child: Padding(
          padding: const EdgeInsets.all(18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Chip(
                            label: SvgIcon(
                                width: 15,
                                height: 15,
                                icon: AppImage.left,
                                onHandle: () {
                                  quillController.updateAlign(
                                      'align', TextAlign.left);
                                }),
                          ),
                          AppSize.sbx(5),
                          Chip(
                              label: SvgIcon(
                                  width: 15,
                                  height: 15,
                                  icon: AppImage.right,
                                  onHandle: () {
                                    print('ruleft');
                                    quillController.updateAlign(
                                        'align', TextAlign.right);
                                  })),
                          AppSize.sbx(5),
                          Chip(
                              label: SvgIcon(
                                  width: 15,
                                  height: 15,
                                  icon: AppImage.center,
                                  onHandle: () {})),
                        ],
                      ),
                    ),
                    AppSize.sbx(10),
                    Expanded(
                      child: Row(
                        children: [
                          Chip(
                              label: SvgIcon(
                                  width: 15,
                                  height: 15,
                                  icon: AppImage.font,
                                  onHandle: () {})),
                          AppSize.sbx(5),
                          Chip(
                              label: SvgIcon(
                                  width: 15,
                                  height: 15,
                                  icon: AppImage.font,
                                  onHandle: () {})),
                          AppSize.sbx(5),
                          Chip(
                              label: SvgIcon(
                                  width: 15,
                                  height: 15,
                                  icon: AppImage.font,
                                  onHandle: () {})),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: 20,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () {},
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: colors[index]),
                      ),
                    ),
                    separatorBuilder: (context, index) => AppSize.sbx(20),
                    itemCount: colors.length,
                  ),
                ),
              ),
              Expanded(
                  child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                ),
                itemBuilder: (context, index) => Chip(
                    label: GestureDetector(
                  onTap: () {},
                  child: Text(fonts[index]),
                )),
                itemCount: fonts.length,
              ))
            ],
          ),
        ),
      ),
    );
  }
}
