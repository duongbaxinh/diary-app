import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IconBottomSheet extends StatelessWidget {
  final String title;

  const IconBottomSheet({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    final List<String> titles = [
      '😄',
      '😅',
      'Classic',
      'Cute',
      'Holiday',
      'Color'
    ];
    final HomeController controller = Get.find();

    List<String> icons = ['😅', '🙂', '🤣', '😍', '😭', '😓'];

    return DefaultTabController(
        length: titles.length,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            leading: const Icon(Icons.arrow_drop_down_outlined),
            title: Text(
              title,
              style: TextStyle(
                fontSize: AppText.heading3,
                fontWeight: AppText.w3,
              ),
            ),
            actions: const [
              Chip(
                label: Icon(Icons.save, color: AppColor.whiteColor),
                backgroundColor: AppColor.primaryColor,
              )
            ],
            bottom: TabBar(
              isScrollable: true,
              padding: AppBorder.padding_00,
              tabs: [
                for (String title in titles)
                  Text(
                    title,
                    style: const TextStyle(
                      fontSize: 13,
                    ),
                  )
              ],
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(18),
            child: TabBarView(
              children: [
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 6,
                    mainAxisSpacing: 5,
                    crossAxisSpacing: 5,
                  ),
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      print(
                          'run at here icon ${controller.diaryModel['title'] + icons[index]}');
                      controller.updateDiaryDiaryModel(
                          controller.diaryModel['title'] + icons[index],
                          'title');
                    },
                    child: Text(icons[index], style: TextStyle(fontSize: 25)),
                  ),
                  itemCount: icons.length,
                )
              ],
            ),
          ),
        ));
  }
}
