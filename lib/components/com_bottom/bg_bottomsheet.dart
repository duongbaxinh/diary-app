import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/controller/main_controller.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BgBottomSheet extends StatelessWidget {
  final String title;
  final RxMap<String, dynamic> bgDiary;
  const BgBottomSheet({super.key, required this.title, required this.bgDiary});

  @override
  Widget build(BuildContext context) {
    final List<String> titles = [
      'All',
      'Simple',
      'Classic',
      'Cute',
      'Holiday',
      'Color'
    ];
    final HomeController controller = Get.find();

    RxList<Map<String, dynamic>> themes = [
      {"bg": AppImage.background_01, "color": Colors.white},
      {"bg": AppImage.background_02, "color": Colors.white},
      {"bg": AppImage.background_03, "color": Colors.white},
      {"bg": AppImage.background_04, "color": Colors.white},
      {"bg": AppImage.background_05, "color": Colors.white},
    ].obs;
    return DefaultTabController(
        length: titles.length,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            leading: const Icon(Icons.arrow_drop_down_outlined),
            title: Text(
              title,
              style: TextStyle(
                fontSize: AppText.heading3,
                fontWeight: AppText.w3,
              ),
            ),
            actions: const [
              Chip(
                label: Icon(Icons.save, color: AppColor.whiteColor),
                backgroundColor: AppColor.primaryColor,
              )
            ],
            bottom: TabBar(
              isScrollable: true,
              padding: AppBorder.padding_00,
              tabs: [
                for (String title in titles)
                  Text(
                    title,
                    style: const TextStyle(
                      fontSize: 13,
                    ),
                  )
              ],
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TabBarView(
              children: [
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 2 / 3),
                  itemBuilder: (context, index) => Obx(() => GestureDetector(
                        onTap: () => {
                          controller.diaryModel['bg'] =
                              themes[index]['bg'].value
                        },
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Image.asset(
                            themes[index]['bg'].value,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )),
                  itemCount: themes.length,
                ),
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 2 / 3),
                  itemBuilder: (context, index) => Obx(() => GestureDetector(
                        onTap: () => {
                          controller.diaryModel['bg'] =
                              themes[index]['bg'].value
                        },
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Image.asset(
                            themes[index]['bg'].value,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )),
                  itemCount: themes.length,
                ),
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 2 / 3),
                  itemBuilder: (context, index) => Obx(() => GestureDetector(
                        onTap: () => {
                          controller.diaryModel['bg'] =
                              themes[index]['bg'].value
                        },
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Image.asset(
                            themes[index]['bg'].value,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )),
                  itemCount: themes.length,
                ),
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 2 / 3),
                  itemBuilder: (context, index) => Obx(() => GestureDetector(
                        onTap: () => {
                          controller.diaryModel['bg'] =
                              themes[index]['bg'].value
                        },
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Image.asset(
                            themes[index]['bg'].value,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )),
                  itemCount: themes.length,
                ),
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 2 / 3),
                  itemBuilder: (context, index) => Obx(() => GestureDetector(
                        onTap: () => {
                          controller.diaryModel['bg'] =
                              themes[index]['bg'].value
                        },
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Image.asset(
                            themes[index]['bg'].value,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )),
                  itemCount: themes.length,
                ),
                GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 2 / 3),
                  itemBuilder: (context, index) => Obx(() => GestureDetector(
                        onTap: () {
                          print('run at here ');
                          controller.diaryModel['bg'] =
                              themes[index]['bg'].value;
                        },
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Image.asset(
                            themes[index]['bg'].value,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )),
                  itemCount: themes.length,
                ),
              ],
            ),
          ),
        ));
  }
}
