import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IconBtn extends StatelessWidget {
  final String icon;
  final VoidCallback onHandel;
  const IconBtn({super.key, required this.icon, required this.onHandel});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 50,
      child: Padding(
        padding: AppBorder.padding_00,
        child: ElevatedButton(
            onPressed: onHandel,
            style: ButtonStyle(
                elevation: MaterialStateProperty.all(0),
                padding: MaterialStateProperty.all(EdgeInsets.all(0)),
                backgroundColor:
                    MaterialStateProperty.all(AppColor.transparent)),
            child: Image(
              image: AssetImage(icon),
              width: double.infinity,
              height: double.infinity,
            )),
      ),
    );
  }
}
