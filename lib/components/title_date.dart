import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:diary_app/utils/format_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TitleDate extends StatelessWidget {
  final Diary diary;
  final String emotion;
  final VoidCallback fn;
  final Color? color;
  final isChangeEmotion;
  const TitleDate(
      {super.key,
      required this.diary,
      this.color = AppColor.darkGreyColor,
      this.isChangeEmotion = false,
      this.emotion = '',
      required this.fn});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      titleAlignment: ListTileTitleAlignment.center,
      style: ListTileStyle.list,
      titleTextStyle: TextStyle(
          fontFamily: AppText.mainFontFamily,
          color: color,
          fontWeight: AppText.w2),
      subtitleTextStyle: TextStyle(
          fontFamily: AppText.mainFontFamily,
          color: color,
          fontWeight: AppText.w2),
      textColor: color,
      leading: Text(
        diary.date.day.toString(),
        style: TextStyle(
            fontSize: AppText.heading1,
            fontWeight: AppText.w1,
            fontFamily: AppText.mainFontFamily),
      ),
      trailing: isChangeEmotion
          ? GestureDetector(
              onTap: fn,
              child: Image.asset(
                emotion == '' ? AppImage.faceIcon_01 : emotion,
                width: 35,
                height: 35,
              ),
            )
          : Image.asset(
              diary.icon,
              width: 35,
              height: 35,
            ),
      title: Row(
        children: [
          Text(FormatDate.monthName(
              DateTime(diary.date.year, diary.date.month))),
          const SizedBox(
            width: 8,
          ),
          Text(diary.date.year.toString())
        ],
      ),
      subtitle: Text(FormatDate.dayName(diary.date)),
      contentPadding: const EdgeInsets.all(0),
    );
  }
}
