import 'dart:io';

import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'com_bottom/diary_bottomsheet.dart';

class SvgIcon extends StatelessWidget {
  final String icon;
  final bool isColor;
  final VoidCallback onHandle;
  final bool special;
  final double width;
  final double height;

  final Color color;
  const SvgIcon(
      {super.key,
      required this.icon,
      required this.onHandle,
      this.color = AppColor.secondColor,
      this.special = false,
      this.isColor = true,
      this.width = 30,
      this.height = 30});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: special
          ? () => {
                Get.bottomSheet(
                    isScrollControlled: true, const DiaryBottomSheet())
              }
          : onHandle,
      child: special
          ? GestureDetector(
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: const [AppColor.boxShadowTemplate],
                    borderRadius: AppBorder.borderCircle),
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: SvgPicture.asset(
                    icon,
                    width: double.infinity,
                    height: double.infinity,
                    color: Colors.red,
                  ),
                ),
              ),
            )
          : SvgPicture.asset(
              icon,
              width: width,
              height: height,
              color: isColor ? color : null,
            ),
    );
  }
}
