import 'package:diary_app/components/icon_button.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DayItem extends StatelessWidget {
  final bool daySelected;
  final DateTime day;
  final List<Diary> diaries;

  const DayItem(
      {super.key,
      required this.daySelected,
      required this.day,
      required this.diaries});

  @override
  Widget build(BuildContext context) {
    Diary? diary;
    if (diaries.isNotEmpty) {
      diary = diaries.firstWhere(
          (element) =>
              day.day == element.date.day &&
              day.month == element.date.month &&
              day.year == element.date.year,
          orElse: () => Diary(date: DateTime(0)));
    }
    DateTime today = DateTime.now();
    return Container(
      margin: const EdgeInsets.all(3),
      width: 10,
      height: 10,
      decoration: BoxDecoration(
          border: Border.all(
              color: daySelected ? AppColor.primaryColor : AppColor.transparent,
              width: 3),
          color: today.day < day.day && today.month <= day.month
              ? Colors.white.withOpacity(0.2)
              : Colors.white,
          shape: BoxShape.circle),
      child: Center(
        child: diary != null &&
                day.day == diary.date.day &&
                day.month == diary.date.month &&
                day.year == diary.date.year
            ? Image.asset(diary.icon)
            : Text(
                day.day.toString(),
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: AppText.heading3,
                    fontFamily: AppText.mainFontFamily),
              ),
      ),
    );
  }
}
