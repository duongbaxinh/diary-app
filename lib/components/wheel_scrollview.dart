import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:diary_app/utils/format_date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WheelScrollView extends StatelessWidget {
  final Function(String, dynamic) onChangeDate;
  const WheelScrollView({super.key, required this.onChangeDate});

  @override
  Widget build(BuildContext context) {
    RxInt currentMonth = DateTime.now().month.obs;
    RxInt currentYear = DateTime.now().year.obs;
    Rx<DateTime> today = DateTime.now().obs;
    void setDone() {
      onChangeDate(
          '',
          DateTime.utc(currentYear.value + 2021, currentMonth.value + 1,
              today.value.day));
      Get.back();
    }

    listItem() {
      List<Widget> ls = [];
      for (int i = 1; i <= 12; i++) {
        ls.add(Text(
          FormatDate.monthName(
              DateTime.utc(today.value.year, i, today.value.day)),
          style: TextStyle(
              fontWeight: AppText.w1,
              fontSize: AppText.heading2,
              fontFamily: AppText.mainFontFamily),
        ));
      }
      return ls;
    }

    listItemYear() {
      List<Widget> ls = [];
      for (int i = 1; i <= 12; i++) {
        ls.add(Text(
          (2020 + i).toString(),
          style: TextStyle(
              fontWeight: AppText.w1,
              fontSize: AppText.heading2,
              fontFamily: AppText.mainFontFamily),
        ));
      }
      return ls;
    }

    return Obx(() {
      return Expanded(
        child: Column(
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: ListWheelScrollView(
                        itemExtent: 50,
                        // onSelectedItemChanged: (value) => fn(value),
                        diameterRatio: 10,
                        perspective: 0.01,
                        physics: const FixedExtentScrollPhysics(),
                        useMagnifier: true,
                        magnification: 1.4,
                        onSelectedItemChanged: (value) =>
                            currentMonth.value = value,
                        children: listItem()),
                  ),
                  Expanded(
                    flex: 1,
                    child: ListWheelScrollView(
                        itemExtent: 50,
                        // onSelectedItemChanged: (value) => fn(value),
                        diameterRatio: 10,
                        perspective: 0.01,
                        physics: const FixedExtentScrollPhysics(),
                        useMagnifier: true,
                        magnification: 1.4,
                        onSelectedItemChanged: (value) =>
                            currentYear.value = value,
                        children: listItemYear()),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: AppSize.maxW(context),
              child: TextButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(AppColor.primaryColor)),
                  onPressed: () => setDone(),
                  child: Text(
                    'Done',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: AppText.heading2,
                        fontWeight: AppText.w1),
                  )),
            )
          ],
        ),
      );
    });
  }
}
