import 'dart:io';

import 'package:diary_app/components/title_date.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_size.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:diary_app/utils/format_date.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DiaryItem extends StatelessWidget {
  final Diary diary;
  const DiaryItem({super.key, required this.diary});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
          color: AppColor.bgColor, borderRadius: AppBorder.radius1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TitleDate(
            fn: () {},
            diary: diary,
          ),
          Text(
            diary.title,
            style:
                TextStyle(fontWeight: AppText.w1, fontSize: AppText.heading1),
          ),
          Text(
            diary.content,
            style:
                TextStyle(fontWeight: AppText.w4, fontSize: AppText.heading4),
          ),
          diary.images.isNotEmpty
              ? SizedBox(
                  width: AppSize.maxW(context),
                  height: 80,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => AspectRatio(
                      aspectRatio: 4 / 6,
                      child: Image.file(
                        File(diary.images[index]),
                        fit: BoxFit.fill,
                      ),
                    ),
                    separatorBuilder: (context, index) => AppSize.sbx(10),
                    itemCount: diary.images.length,
                  ),
                )
              : Text('')
        ],
      ),
    );
  }
}
