import 'dart:ffi';

import 'package:diary_app/controller/quill_controller.dart';
import 'package:diary_app/styles/app_border.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TextInput extends StatelessWidget {
  final double fontSize;
  final int max_line;
  final int min_line;
  final String text;
  final FontWeight wHintext;
  final Color color;
  final FontWeight wContent;
  final Function(String) onChangeVa;
  const TextInput(
      {super.key,
      this.fontSize = 25,
      this.wHintext = FontWeight.w900,
      this.wContent = FontWeight.w500,
      required this.onChangeVa,
      required this.max_line,
      required this.min_line,
      this.color = Colors.black,
      this.text = ''});

  @override
  Widget build(BuildContext context) {
    final QuillController quillController = Get.put(QuillController());
    return TextFormField(
      onChanged: (value) => onChangeVa(value),
      initialValue: text,
      style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontFamily: AppText.mainFontFamily,
          fontWeight: wContent),
      decoration: InputDecoration(
          contentPadding: AppBorder.padding_00,
          border: const OutlineInputBorder(borderSide: BorderSide.none),
          hintText: 'Start typing here...',
          hintStyle: TextStyle(
              fontSize: fontSize,
              color: color,
              fontFamily: AppText.mainFontFamily,
              fontWeight: wHintext)),
    );
  }
}
