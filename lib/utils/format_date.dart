import 'package:intl/intl.dart';

class FormatDate {
  static String Function(DateTime date) dayName =
      (date) => DateFormat('EEEE').format(date);
  static String Function(DateTime date) monthName =
      (date) => DateFormat('MMMM').format(date);
}
