import 'package:diary_app/model/Diary.dart';

class ProcessingDate {
  static dias(DateTime date, List<Diary> diaries) {
    List<Diary> ls = [];
    for (final di in diaries) {
      if (di.date.day == date.day &&
          di.date.month == date.month &&
          di.date.year == date.year) {
        ls.add(di);
      }
    }
    return ls;
  }
}
