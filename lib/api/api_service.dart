import 'package:get/get.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ApiService {
  Future<void> addOneWorkPlan(Map<String, dynamic> diary) async {
    FirebaseFirestore fireStore = FirebaseFirestore.instance;
    CollectionReference diary = fireStore.collection('diary');
    DocumentReference newDiary = await diary.add(diary);
    print('add diary successfully');
  }
}
