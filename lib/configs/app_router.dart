import 'package:diary_app/pages/gallery/index.dart';
import 'package:diary_app/pages/home_page/index.dart';
import 'package:diary_app/pages/main_page/index.dart';
import 'package:diary_app/pages/mine_page/index.dart';
import 'package:diary_app/pages/my_diary/index.dart';
import 'package:diary_app/pages/splash_page/index.dart';

class AppRoutes {
  static const splash = '/splash';
  static const home = '/home';
  static const myDiary = '/my_diary';
  static const calender = '/calender';
  static const gallery = '/gallery';
  static const mine = '/mine';
  static const main = '/main';
  static final pages = {
    AppRoutes.main: (context) => const MainPage(),
    AppRoutes.home: (context) => const HomePage(),
    AppRoutes.splash: (context) => const SplashScreen(),
    AppRoutes.mine: (context) => const MinePage(),
    AppRoutes.gallery: (context) => const Gallery(),
    AppRoutes.myDiary: (context) => const MyDiary(),
  };
}
