import 'package:get/get.dart';

class AppImage {
  static var background_01 = 'assets/images/background_01.jpg'.obs;
  static var background_02 = 'assets/images/background_02.jpg'.obs;
  static var background_03 = 'assets/images/background_03.jpg'.obs;
  static var background_04 = 'assets/images/background_04.jpg'.obs;
  static var background_05 = 'assets/images/background_05.jpg'.obs;

  static String faceIcon_01 = 'assets/icons/iconface_01.png';
  static String faceIcon_02 = 'assets/icons/iconface_02.png';
  static String faceIcon_03 = 'assets/icons/iconface_03.png';
  static String faceIcon_04 = 'assets/icons/iconface_04.png';
  static String faceIcon_05 = 'assets/icons/iconface_05.png';
  static String faceIcon_06 = 'assets/icons/iconface_06.png';
  static String icon_app = 'assets/icons/icon_app.png';

  // ----------------------- Start Icon emotion --------------------------------
  static String angry = 'assets/svgs/icon_angry.svg';
  static String haha = 'assets/svgs/icon_haha.svg';
  static String love = 'assets/svgs/icon_love.svg';
  static String sad = 'assets/svgs/icon_sad.svg';
  static String wow = 'assets/svgs/icon_wow.svg';

  // ----------------------- End Icon emotion ----------------------------------

  static String account = 'assets/svgs/icon_account.svg';
  static String arrowDown = 'assets/svgs/icon_down.svg';
  static String calendar = 'assets/svgs/icon_calendar.svg';
  static String diary = 'assets/svgs/icon_diary.svg';
  static String faq = 'assets/svgs/icon_faq.svg';
  static String feedback = 'assets/svgs/icon_feedback.svg';
  static String language = 'assets/svgs/icon_language.svg';
  static String mood = 'assets/svgs/icon_mood.svg';
  static String password = 'assets/svgs/icon_password.svg';
  static String photo = 'assets/svgs/icon_photo.svg';
  static String policy = 'assets/svgs/icon_policy.svg';
  static String rate = 'assets/svgs/icon_rate.svg';
  static String search = 'assets/svgs/icon_search.svg';
  static String threedot = 'assets/svgs/icon_threedot.svg';
  static String trash = 'assets/svgs/icon_trash.svg';
  static String add = 'assets/svgs/icon_add.svg';
  static String background = 'assets/svgs/icon_bg.svg';
  static String emotion = 'assets/svgs/icon_emotion.svg';
  static String textChange = 'assets/svgs/icon_textChange.svg';
  static String tag = 'assets/svgs/icon_tag.svg';
  static String left = 'assets/svgs/icon_left.svg';
  static String right = 'assets/svgs/icon_right.svg';
  static String center = 'assets/svgs/icon_center.svg';
  static String font = 'assets/svgs/icon_font.svg';
  static String edit = 'assets/svgs/icon_edit.svg';
}
