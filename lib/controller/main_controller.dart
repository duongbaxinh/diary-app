import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diary_app/configs/app_image.dart';
import 'package:diary_app/model/Diary.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String THEME_DIARY = "themediary";

class HomeController extends GetxController {
  RxList<Diary> listDiary = [Diary(date: DateTime.now())].obs;
  RxBool isSetPin = false.obs;

  List<RxString> backgrounds = [
    AppImage.background_01.toString().obs,
    AppImage.background_02.toString().obs,
    AppImage.background_03.toString().obs,
    AppImage.background_04.toString().obs,
    AppImage.background_05.toString().obs,
  ];
  RxMap<String, dynamic> theme = {"background": '', "color": ''}.obs;
  // khởi tạo form data để lưu trữ dữ liệu diary
  RxMap<String, dynamic> diaryModel = {
    "title": '',
    "content": '',
    "icon": AppImage.faceIcon_01,
    "date": DateTime.now(),
    "image": [],
    "bg": AppImage.background_01.value
  }.obs;
  @override
  void onInit() {
    getDiaryTheme();
    getListDiary();
    super.onInit();
  }

  void changeBackground(backgroundChange) {
    theme['background'] = backgrounds[backgroundChange].value;
    updateDiaryTheme(theme);
  }

  Future<SharedPreferences> initShareData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> initTheme = {
      "background": AppImage.background_01.value,
      "color": ''
    };
    String checkTheme = prefs.getString(THEME_DIARY) ?? '';
    if (checkTheme.isEmpty) {
      String dataJson = jsonEncode(initTheme);
      prefs.setString(THEME_DIARY, dataJson);
    }
    return prefs;
  }

  Future<void> setPin(String pin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (pin.length == 4) {
      prefs.setString('pin', pin);
    } else {
      return;
    }
  }

  Future<bool> validate(String pin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('pin', '1234');
    bool check = prefs.getBool('setpin') ?? false;
    print('checkpin ${prefs.getString('pin') == pin}');
    if (check) {
      return prefs.getString('pin') == pin;
    } else {
      setPin(pin);
      prefs.setBool('setpin', true);
      return true;
    }
  }

  updateDiaryTheme(Map<String, dynamic> diaryTheme) async {
    SharedPreferences presh = await initShareData();
    String dataJson = jsonEncode(diaryTheme);
    presh.setString(THEME_DIARY, dataJson);
  }

  removeDiaryImage(index) {
    diaryModel['image'].removeAt(index);
    update();
  }

  updateDiaryDiaryModel(dynamic value, String key) {
    print('check value ${value[0]}');
    if (key == 'image') {
      diaryModel['image'].add(value[0]);
    } else {
      diaryModel[key] = value;
    }
  }

  Future<void> getDiaryTheme() async {
    SharedPreferences presh = await initShareData();
    String dataJson = presh.getString(THEME_DIARY) ?? '';
    Map<String, dynamic> dataMap = jsonDecode(dataJson);
    updateDiaryDiaryModel(dataMap['background'], 'background');
    theme['background'] = dataMap['background'];
    theme['color'] = dataMap['color'];
  }

//   ------------------------------------ work with firebase -----------------------------
  Future<void> addOneWorkPlan() async {
    try {
      print("run at here ::: add mode");

      FirebaseFirestore fireStore = FirebaseFirestore.instance;
      CollectionReference<Map<String, dynamic>> lessonRef =
          fireStore.collection('diary');
      QuerySnapshot<Map<String, dynamic>> lessonSnapshot =
          await lessonRef.get();
      print('check ${lessonSnapshot}');
      CollectionReference diary = fireStore.collection('diary');
      DocumentReference newDiary = await diary.add(diaryModel.value);
      print('add diary successfully');
      getListDiary();
    } on Exception catch (e) {
      print('check error $e');
    }
  }

  Future<List<Diary>> getListDiary() async {
    try {
      listDiary.value = [];
      FirebaseFirestore firestore = FirebaseFirestore.instance;
      CollectionReference<Map<String, dynamic>> diaries =
          firestore.collection('diary');
      QuerySnapshot<Map<String, dynamic>> items = await diaries.get();
      for (var item in items.docs) {
        Map<String, dynamic> data = item.data();
        data['id'] = item.id;
        listDiary.add(Diary.fromJson(data));
      }
      print('data $listDiary');
      return [];
    } on Exception catch (e) {
      print('error :::: $e');
    }
    return [];
  }

  void updateDocument(idDoc) async {
    print('check id ${idDoc}');
    await FirebaseFirestore.instance
        .collection('diary')
        .doc(idDoc)
        .update(diaryModel.value);
    getListDiary();
  }

  void deleteDiary(idDoc) async {
    print('check id ${idDoc}');
    await FirebaseFirestore.instance.collection('diary').doc(idDoc).delete();
    getListDiary();
  }
}
