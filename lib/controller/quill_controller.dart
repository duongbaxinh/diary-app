import 'package:diary_app/styles/app_color.dart';
import 'package:diary_app/styles/app_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class QuillController extends GetxController {
  Map<String, dynamic> align = {
    "LEFT": TextAlign.left,
    "RIGHT": TextAlign.right,
    "CENTER": TextAlign.center,
  };
  Map<String, dynamic> color = {
    "LEFT": TextAlign.left,
    "RIGHT": TextAlign.right,
    "CENTER": TextAlign.center,
  };
  RxMap<String, dynamic> font = {
    "align": TextAlign.left,
    "color": AppColor.darkGreyColor,
    "font": AppText.heading3,
  }.obs;

  updateAlign(String key, dynamic value) {
    font.value[key] = value;
    update();
  }
}
