class Diary {
  late String id;
  late String title;
  late String content;
  late String icon;
  late List<dynamic> images;
  late DateTime date;
  late String background;
  Diary(
      {this.id = '',
      this.title = '',
      this.content = '',
      this.icon = '',
      this.images = const [],
      required this.date,
      this.background = ''});

  Diary.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    content = json['content'];
    icon = json['icon'];
    images = json['image'];
    background = json['bg'];
    date = json['date'].toDate();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['content'] = content;
    data['icon'] = icon;
    data['image'] = images;
    data['bg'] = background;
    data['date'] = date;
    return data;
  }

  @override
  String toString() {
    return 'Diary{title: $title, content: $content, icon: $icon, images: $images, date: $date, background: $background}';
  }
}
