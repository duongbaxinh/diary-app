import 'package:flutter/material.dart';

class AppSize {
  static double Function(dynamic context) maxW =
      (context) => MediaQuery.of(context).size.width;
  static double Function(dynamic context) maxH =
      (context) => MediaQuery.of(context).size.height;
  static Widget Function(double size) sbx = (size) => SizedBox(width: size);
  static Widget Function(double size) sby = (size) => SizedBox(height: size);
}
