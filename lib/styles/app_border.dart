import 'package:flutter/material.dart';

class AppBorder {
  static BorderRadius radius1 = BorderRadius.circular(15);
  static BorderRadius radius2 = BorderRadius.circular(30);
  static BorderRadius radius3 = BorderRadius.circular(40);

  static BorderRadius borderTLTR = const BorderRadius.only(
      topLeft: Radius.circular(20), topRight: Radius.circular(20));
  static BorderRadius borderBLBR = const BorderRadius.only(
      bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15));
  static BorderRadius borderCircle = BorderRadius.circular(99999999);

  static EdgeInsets padding_00 = const EdgeInsets.all(0);
  static EdgeInsets padding_01 = const EdgeInsets.all(10);
  static EdgeInsets padding_02 = const EdgeInsets.all(15);
  static EdgeInsets padding_03 = const EdgeInsets.all(20);
  static EdgeInsets padding_04 = const EdgeInsets.all(25);

  static EdgeInsets padding_01_03 =
      const EdgeInsets.symmetric(vertical: 10, horizontal: 20);
}
