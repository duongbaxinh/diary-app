import 'package:flutter/cupertino.dart';

class AppText {
  static String mainFontFamily = 'DancingScript';

  static double heading1 = 25;
  static double heading2 = 20;
  static double heading3 = 18;
  static double heading4 = 15;
  static double heading5 = 12;

  static FontWeight w1 = FontWeight.w900;
  static FontWeight w2 = FontWeight.w800;
  static FontWeight w3 = FontWeight.w700;
  static FontWeight w4 = FontWeight.w600;
  static FontWeight w5 = FontWeight.w500;
  static FontWeight w6 = FontWeight.w400;
  static FontWeight w7 = FontWeight.w300;
}
