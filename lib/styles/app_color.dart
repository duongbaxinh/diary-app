import 'package:flutter/material.dart';

class AppColor {
  static const primaryColor = Color(0xFFFF1A66);
  static const secondColor = Color(0xFF0D0D0D);
  static const darkGreyColor = Color(0xFF999999);
  static const orangeColor = Color(0xFFFFA64D);
  static const whiteColor = Color(0xFFFFFFFF);
  static const bgColor = Color(0xFFFFE6FF);
  static const transparent = Colors.transparent;

  static const boxShadowTemplate =
      BoxShadow(color: primaryColor, blurRadius: 10, offset: Offset(0, 0));

  void changeColor(type) {}
}
